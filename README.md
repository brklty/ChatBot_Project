# ChatBot_Project

## Schritte:

- Datenimport und -upload
- Vorbereitung der Daten
- Erstellung von Trainings- und Testdaten
- Erstellen des Modells
- Vorhersage der Antwort


## Die Dateistruktur und die Art der Dateien:

- Intents.json 

Datendatei mit vordefinierten Mustern und Antworten.

- chat_model.py

Ein Skript zum Erstellen des Modells und zum Trainieren des Chatbots

- words.pkl

Eine pkl-Datei, in der die Wörter des Python-Objekts gespeichert sind, das die Vokabelliste enthält.

- classes.pkl

pkl-Datei mit Liste der Kategorien.


- chatbot_model.h5

Ein trainiertes Modell, das Informationen über das Modell enthält und die Gewichte der Neuronen hat.


- chatbot_gui.py

Es ist eine in Python-Skript implementierte GUI für Chatbots.
