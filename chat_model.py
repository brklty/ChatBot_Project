import nltk
from nltk.stem import WordNetLemmatizer
import json
import pickle
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
import tensorflow as tf
optimizer=tf.keras.optimizers.SGD()


wnl = WordNetLemmatizer()
words=[]
classes = []
documents = []
ignore_words = ['?', '!']
data_file = open('intents.json').read()
intents = json.loads(data_file)


#Vorbereitung
#Tokenizing
for intent in intents['intents']:
    for pattern in intent['pattern']:
        #tokenize each word
        w = nltk.word_tokenize(pattern)
        words.extend(w)
        #add documents in the corpus
        documents.append((w, intent['tag']))
        # add to our classes list
        if intent['tag'] not in classes:
            classes.append(intent['tag'])


#BMI berechnen
def calculate_BMI():
    height = float(input("Was ist Ihre Größe? (cm) ?"))
    weight = float(input("Was ist Ihr Gewicht? (kg) ?"))
    BMI = weight/(height/100)**2
    return BMI


    #BMI definirien
def define_BMI(BMI):
    if BMI < 17.5:
        print("Kritisches Untergewichtt")
    elif 17.5 < BMI < 22:
        print("Untergewicht")
    elif 22 < BMI < 28:
        print("Normalgewicht")
    elif 28 < BMI < 33:
        print("Leichtes Übergewicht")
    else:
        print("Übergewichte")